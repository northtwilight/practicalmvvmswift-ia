//
//  CarViewModel.swift
//  PracticalMVVMCandyCode
//
//  Created by Massimo Savino on 2017-03-04.
//  Copyright © 2017 Massimo Savino. All rights reserved.
//

import Foundation


class CarViewModel {
    
    private var car: Car
    static let horsepowerPerKilowatt = 1.34102209
    
    var modelText: String? {
    
        return car.model
    }
    
    var makeText: String? {
        
        return car.make
    }
    
    var horsepowerText: String? {
        
        let horsepower = Int(round(Double(car.kilowatts) * CarViewModel.horsepowerPerKilowatt))
        
        return "\(horsepower) HP"
    }
    
    var titleText: String? {
        
        return "\(car.make) \(car.model)"
    }
    
    var photoURL: NSURL? {
        
        return NSURL(string: car.photoURL)
    }
    
    
    
    init(car: Car) {
        
        self.car = car
        
    }
}
