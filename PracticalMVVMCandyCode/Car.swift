//
//  Car.swift
//  PracticalMVVMCandyCode
//
//  Created by Massimo Savino on 2017-03-04.
//  Copyright © 2017 Massimo Savino. All rights reserved.
//

import Foundation


class Car {
    
    var model: String
    var make: String
    var kilowatts: Int
    var photoURL: String
    
    init(model: String, make: String, kilowatts: Int, photoURL: String) {
        
        self.model = model
        self.make = make
        self.kilowatts = kilowatts
        self.photoURL = photoURL
        
    }
}
